= KTDK Overview

This parts describes the main idea behind KTDK and how each components are supposed to work.

== Abstract

KTDK is not supposed to run as it is, it is mostly the library and it should be called by executor.

link:https://gitlab.fi.muni.cz/grp-kontr2/ktdk-executor[Executor] is program implemented in python.

KTDK has been implemented in python 3.6.

The execution consists of 3 main phases:
- Definition (Define Tests, Tasks and Assertions)
- Execution (Execute defined Tests and Tasks)
- Process (Process the test results)


== Components

This section will describe all the components, relationships between them and how to use them.

=== KTDK
`KTDK` is the main object, that should contain the configuration and Test's root called the `suite`.

Example how to get KTDK instance:
[source,python]
----
ktdk = KTDK.get_instance()
----

---

=== Test

Test defines the namespace for other tests and tasks.
Test consists of tests and tasks. Tests in test has tree structure. Each Test can contain 
arbitrary number of other tests.

`Suite` (ie. `ktdk.suite`) is type of `Test` and it is the root of this tree.

For each test you can define:
- name
- description
- weight
- tasks
- tags
- child tests
- runner (will be described later - TLDR: How you would like to execute tests and tasks subtree)

Example:

[source, python]
----
naostro = Test(name="naostro", desc="Test nanecisto", tags=['naostro'])
nanecisto = Test(name="nanecisto", desc="Test nanecisto", tags=['nanecisto'])
hello_test = Test(name="hello_test", desc="Super hello test")

naostro.add_test(hello_test)

ktdk.suite.add_test(naostro)
ktdk.suite.add_test(nanecisto)
----

This example creates two main tests: `naostro` a `nanecisto`, these two tests are beeing added
to the suite at the end of the example. *Naostro* test will contain `hello_test`.

==== Test tags

Tags defines which tests should be executed.
For tags evaluation there are multiple rules:

- Each child will **inherit** it's parent's tags.
(i.e. if `naostro` has tag `A` all it's subtests are threated like it has tag `A`)
- To get all the tags (inherited or self), you can use the `effective_tags` methods
- When you do not specify any tag, all the tests will be executed
- When you specify multiple tags, the intersection between `effective_tags` for test and the provided set has to be not empty.
- When you provide negated (**excluded**) tag or tags, all the tests, which `effective_tags` intersect with **excluded** tags set will be omited (**SKIPED**).
- Excluded tags has always higher priority


==== Test weight

Test weight is floating point number where `1` is `100%`, the algorithm is kind of tricky. 
Here are the rules:

- If no weight has been specified - the weight of the test is `1`
- Root test (suite) has always weight `1.0`
- There is `effective_weight` that is computed as: 

[source, python]
----
    TBD
----




=== Tasks
Tasks are the components like tests, can have name, description, but no weight and also they can implement `run()` method. Run method defines action to be executed when task is executed.

Tasks can also be structured in the tree structure.

Each task contains `asserts` array. Asserts contains result of assertions (checks and requires).
Task containts methods `check` and `require` methods that are part of `AssertionSupport` (Mixin)
from which `Task` **inherits**, these methods can be call within the task methods, but not on the instance.
Difference between them is that, require will throw an exception and ends current test, and subtasks and subtests will not be executed.


Example:

[source, python]
----
execute_hello = Task('hello_execute')
execute_hello.add_task(Compile())
execute_hello.add_task(Run())

test_naostro.add_task(execute_hello)
----

On this example there is, `execute_hello`, that is a root task. It contains sequence of the tasks:
`Compile` and `Run`. After that, execute_hello is beeing added to `test_naostro`test_naostro`.
This is just an example.

==== Types of tasks
There are 2 main types of tasks:
- **check tasks**
- **action tasks**

===== Action Tasks

Action tasks are being used to do some actions. Like compile or run executables ...

===== Check Tasks

Check tasks are used to check the results of the action task or some other things.
Check tasks are being added using `check_that` or `require_that`

Example:
[source, python]
----
cmake = CMakeBuildTask(source='src')
cmake.check_that(TestResultCheck(matcher=matchers.ResultPassed()))
ktdk.suite.add_task(cmake)
----


